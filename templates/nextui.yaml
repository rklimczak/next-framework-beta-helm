apiVersion: apps/v1
kind: Deployment
metadata:
  name: {{ .Release.Name }}-ui
  labels:
    app: {{ .Release.Name }}-ui
spec:
  selector:
    matchLabels:
      app: {{ .Release.Name }}-ui
  replicas: {{ .Values.next.replicas.ui }}
  template:
    metadata:
      labels:
        app: {{ .Release.Name }}-ui
    spec:
      containers:
      - name: {{ .Release.Name }}-ui
        image: {{ .Values.next.image }}
        imagePullPolicy: IfNotPresent
        ports:
        - containerPort: 3000
        env:
        - name: CONT_NAME
          value: tupix_tupix.1.xxxxx
        {{- if .Values.proxy.host }}
        - name: PROXY_HOST
          value: {{ .Values.proxy.host | quote }}
        - name: PROXY_PORT
          value: {{ .Values.proxy.port | quote }}
        - name: HTTP_PROXY
          value: {{ template "http_proxy" . }}
        - name: HTTPS_PROXY
          value: {{ template "http_proxy" . }}
        - name: NO_PROXY
          value: {{ .Values.proxy.no_proxy | quote }}
        - name: http_proxy
          value: {{ template "http_proxy" . }}
        - name: https_proxy
          value: {{ template "http_proxy" . }}
        - name: no_proxy
          value: {{ .Values.proxy.no_proxy | quote }}
        {{- end }}
        - name: OPENID_CLIENT
          valueFrom:
            secretKeyRef:
              name: {{ .Release.Name }}-sso-secret
              key: openid_client
        - name: OPENID_SECRET
          valueFrom:
            secretKeyRef:
              name: {{ .Release.Name }}-sso-secret
              key: openid_secret
        {{- if .Values.next.logout }}
        - name: LOGOUT_PAGE
          value: 'https://w3id.sso.ibm.com/pkmslogout'
        {{- end }}
        - name: REDIS_HOST
          value: {{ .Release.Name }}-redis-master
        - name: REDIS_PASSWORD
          valueFrom:
            secretKeyRef:
              name: {{ .Release.Name }}-redis-secret
              key: auth
        - name: MYSQL_HOST
          value: {{ .Release.Name }}-db
        - name: MYSQL_PORT
          value: "3306"
        - name: MYSQL_USER
          value: {{ .Values.db.db.user }}
        - name: MYSQL_PASSWORD
          valueFrom:
            secretKeyRef:
              name: {{ .Release.Name }}-db-secret
              key: mariadb-password
        - name: MYSQL_DATABASE
          value: {{ .Values.db.db.name }}
        - name: DB_CLEANUP_INTERVAL
          value: {{ .Values.db.cleanup_interval | quote }}
        - name: RAILS_ENV
          value: {{ .Values.next.rails_env }}
        - name: RAILS_LOG_TO_STDOUT
          value: 'yes'
        - name: RAILS_MASTER_KEY
          value: {{ randAlphaNum 32 | quote }}
        - name: SECRET_KEY_BASE
          valueFrom:
            secretKeyRef:
              name: {{ .Release.Name }}-rails-secret
              key: secret_key_base
        - name: GIT_REPOSITORY
          value: {{ .Values.next.git.repository }}
        - name: GIT_BRANCH
          value: {{ .Values.next.git.branch }}
        - name: GIT_WORKDIR
          value: '/app/tmp'
        - name: ANSIBLE_URL
          value: "https://{{ .Release.Name }}-ansible:5001"
        - name: NR_LICENSE
          value: {{ .Values.next.newrelic_licence_key }}
        - name: SEED
          valueFrom:
            secretKeyRef:
              name: {{ .Release.Name }}-seed-secret
              key: seed
        - name: APPVERSION
          value: {{ .Chart.AppVersion | quote }}
        - name: HAS_LDAP
          value: {{ .Values.ldap.enabled |quote }}
        - name: HAS_LDAP_ENCRYPTION
          value: {{ .Values.ldap.encryption | quote }}
        - name: LDAP_HOST
          value: {{ .Values.ldap.hostname }}
        - name: LDAP_PORT
          value: {{ .Values.ldap.port | quote }}
        - name: LDAP_DN_TEMPLATE
          value: {{ .Values.ldap.dn_template }}
        {{- if .Values.ldap.enable_authenticated_ldap }}
        - name: LDAP_BIND_DN
          value: {{ .Values.ldap.bind_dn }}
        - name: LDAP_BIND_PW
          valueFrom:
            secretKeyRef:
              name: {{ .Release.Name }}-ldap-secret
              key: bind_pw
        {{- end }}
        - name: LDAP_FILTER_USER_ATTR
          value: {{ .Values.ldap.filter_user_attr }}
        {{- if .Values.next.no_ssl }}
          {{- if not .Values.deploy_ca_certificates }}
        - name: NO_SSL
          value: "yes"
          {{- end }}
        {{- end }}
        {{- if and .Values.openid.base_url }}
        - name: W3ID_SITE
          value: {{ .Values.openid.base_url }}
        - name: W3ID_AUTHORIZE_URL
          value: {{ .Values.openid.authorize_url }}
        - name: W3ID_TOKEN_URL
          value: {{ .Values.openid.token_url }}
        - name: W3ID_INTROSPECT_URL
          value: {{ .Values.openid.introspect_url }}
        {{- end }}
        - name: AUTH_TOKEN_LENGTH
          value: {{ .Values.next.auth_token_length | quote }}
        - name: HOSTNAME_REGEX
          value: {{ .Values.next.hostname_regex | quote }}
        volumeMounts:
        - name: {{ .Release.Name }}-certificate-secret
          mountPath: /app/config/certs
          readOnly: true
        - name: {{ .Release.Name }}-repo-secret
          mountPath: /app/config/repokey
          readOnly: true
        readinessProbe:
          tcpSocket:
            port: 3000
          initialDelaySeconds: 120
          periodSeconds: 15
        livenessProbe:
          tcpSocket:
            port: 3000
          initialDelaySeconds: 120
          periodSeconds: 15
      securityContext:
        runAsUser: {{ .Values.next.securityContext.runAsUser }}
      volumes:
      - name: {{ .Release.Name }}-certificate-secret
        secret:
          secretName: {{ .Release.Name }}-certificate-secret
      - name: {{ .Release.Name }}-repo-secret
        secret:
          secretName: {{ .Release.Name }}-repo-secret
      imagePullSecrets:
      {{- if .Values.imageCredentials.existentImageCredentials }}
      - name: {{ .Values.imageCredentials.existentImageCredentials }}
      {{- else }}
      - name: {{ .Release.Name }}-registry-secret
      {{- end }}

---

apiVersion: v1
kind: Service
metadata:
  name: {{ .Release.Name }}-ui
  labels:
    app: {{ .Release.Name }}-ui
spec:
  ports:
  - port: 443
    targetPort: 3000
    protocol: TCP
    name: https
  selector:
    app: {{ .Release.Name }}-ui
